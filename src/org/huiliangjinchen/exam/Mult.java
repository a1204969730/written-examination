package org.huiliangjinchen.exam;

/**
 * ClassName: Mult
 * Description:
 *
 * @Author: zhengyunhe
 * @Date: 2021/2/7 11:40
 * @Version: 1.0
 */
public class Mult implements Calculator{

    @Override
    public int execute(int a, int b) {
        return a*b;
    }
}
