package org.huiliangjinchen.exam;

/**
 * InterfaceName: Calculator
 * Description:
 *
 * @Author: zhengyunhe
 * @Date: 2021/2/7 11:38
 * @Version: 1.0
 */
public interface Calculator {

    int execute(int a,int b);

}
