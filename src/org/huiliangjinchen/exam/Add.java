package org.huiliangjinchen.exam;

/**
 * ClassName: Add
 * Description:
 *
 * @Author: zhengyunhe
 * @Date: 2021/2/7 11:39
 * @Version: 1.0
 */
public class Add implements Calculator{

    @Override
    public int execute(int a, int b) {
        return a+b;
    }
}
