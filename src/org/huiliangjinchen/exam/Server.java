package org.huiliangjinchen.exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: Server
 * Description:
 *
 * @Author: zhengyunhe
 * @Date: 2021/2/7 11:13
 * @Version: 1.0
 */
public class Server {

    public static void main(String[] args) {
        try {
            ServerSocket ss = new ServerSocket(80);

            Map<String,Object> mapping = new HashMap<>();

            mapping.put("add",new Add());
            mapping.put("mult",new Mult());

            while (true) {

                // 实例化客户端
                Socket socket = ss.accept();
                // 获取客户端输入流
                BufferedReader bd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String request;
                int res=0;
                while ((request = bd.readLine()) != null && !request.isEmpty()) {
                    System.out.println(request);
                    if (request.startsWith("GET")) {
                        int pathBegin = request.indexOf("/")+1;
                        int pathEnd = request.indexOf("?");
                        int end = request.indexOf("HTTP/");
                        String path;
                        if (pathEnd != -1){
                            path = request.substring(pathBegin,pathEnd);
                        }else {
                            path = request.substring(pathBegin, end);
                        }
                        Calculator calculator = (Calculator) mapping.get(path);
                        if (calculator == null){
                            continue;
                        }
                        Map<String,Object> params = new HashMap<>();
                        String[] split=request.substring(pathEnd+1,end-1).split("&");
                        for(String s:split){
                            if(s.indexOf("=")>0){
                                params.put(s.substring(0, s.indexOf("=")),s.substring(s.indexOf("=")+1, s.length()));
                            }
                        }
                        try {
                            res = calculator.execute(Integer.parseInt(params.get("a").toString()),Integer.parseInt(params.get("b").toString()));
                        }catch (Exception ignored){
                        }
                    }
                }
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                pw.println("HTTP/1.1 200 OK");
                pw.println("Content-type:text/html");
                pw.println();
                pw.println("<h1>" + res + "</h1>");
                pw.flush();
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }





}
